
const express = require("express");
const app = express();
const path = require("path");
const port = 8000;

app.use(express.static("views"));
app.get("/", (req, res) => {
  console.log(__dirname);
  res.sendFile(__dirname + "/views/pizza365index.html")
})
app.listen(port, () => {
  console.log("App listening on port " + port)
})